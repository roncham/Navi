module.exports = {
    APP_NAME: 'navi',
    API_SERVER: '',
    REGISTER_PATH: '/users/register',
    LOGIN_PATH: '/users/login',
    RESET_PATH: '/users/reset-password',
    SEARCH_HISTORY_PATH: '/search/history',
    SAVED_PINS_PATH: '/search/savedpins',
};